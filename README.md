##User

###Регистарция заявки:
- POST: http://localhost:8080/requests/[userId]
- BODY:
 
`
{
	"requestName": "Добавить задачу",
	"bid": 200,
	"dueDate": "01/01/2019"
}
`

###Получить список заявок:
- GET: http://localhost:8080/requests/[userId]

##Admin

###Получать список всех заявок:

- GET: http://localhost:8080/requests/[userId]

###Возможность выполнять\отказывать конкретную заявку

- PUT: http://localhost:8080/requests/[userId]/[requestId]

`
{
	"requestName": "задачу",
	"bid": 300,
	"dueDate": "нваря",
	"complited": true,
	"cancelled": false
}`

###Cтатистика выполненных\отказанных заявок

####Выполненные
- GET: http://localhost:8080/requests/[userId]/complited

####Невыполненные
- GET: http://localhost:8080/requests/[userId]/not-complited

####Отмененные
- GET: http://localhost:8080/requests/[userId]/cancelled

####Неотмененные
- GET: http://localhost:8080/requests/[userId]/not-cancelled

`[userId]` - id юзера или админа из монго БД. Если админ, то одни полномочия, если юзер - другие.
В задании не было сказано про регистрацию/авторизацию.

123