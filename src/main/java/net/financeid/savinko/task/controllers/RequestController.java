package net.financeid.savinko.task.controllers;

import net.financeid.savinko.task.models.Request;
import net.financeid.savinko.task.models.User;
import net.financeid.savinko.task.repositories.RequestRepository;
import net.financeid.savinko.task.repositories.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/requests")
public class RequestController {

    private RequestRepository requestRepository;
    private UserRepository userRepository;

    public RequestController(RequestRepository requestRepository, UserRepository userRepository) {
        this.requestRepository = requestRepository;
        this.userRepository = userRepository;
    }

    // works
    @GetMapping("/{userId}")
    public List<Request> getAll(@PathVariable("userId") ObjectId userId) {

        User user = this.userRepository.findById(userId);

        // если пользователь - админ, то возвращается список всех заявок
        if (user.isAdmin()) {
            List<Request> foundRequests = this.requestRepository.findAll();
            return foundRequests;
        }

        // если пользователь - простой юзер, то возвращается только списко тех заявок, что закреплены за пользователем
        List<Request> requestsByAuthor_id = this.requestRepository.findRequestsByAuthor_Id(userId.toString());
        return requestsByAuthor_id;
    }

    @GetMapping("/{userId}/complited")
    public List<Request> getAllComplited(@PathVariable("userId") ObjectId userId, HttpServletResponse response) {

        User foundUser = this.userRepository.findById(userId);
        if (!foundUser.isAdmin()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        return this.requestRepository.findRequestsByComplitedIsTrue();
    }

    @GetMapping("/{userId}/not-complited")
    public List<Request> getAllNotComplited(@PathVariable("userId") ObjectId userId, HttpServletResponse response) {

        User foundUser = this.userRepository.findById(userId);
        if (!foundUser.isAdmin()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        return this.requestRepository.findRequestsByComplitedIsFalse();
    }

    @GetMapping("/{userId}/cancelled")
    public List<Request> getAllCancelled(@PathVariable("userId") ObjectId userId, HttpServletResponse response) {

        User foundUser = this.userRepository.findById(userId);
        if (!foundUser.isAdmin()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        return this.requestRepository.findRequestsByCancelledIsTrue();
    }

    @GetMapping("/{userId}/not-cancelled")
    public List<Request> getAllNotCancelled(@PathVariable("userId") ObjectId userId, HttpServletResponse response) {

        User foundUser = this.userRepository.findById(userId);
        if (!foundUser.isAdmin()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        return this.requestRepository.findRequestsByCancelledIsFalse();
    }

    // works
    @PostMapping("/{userId}")
    public Request createRequest(@RequestBody Request request, @PathVariable("userId") ObjectId userId) {

        User foundUser = this.userRepository.findById(userId);
        Request newRequest = new Request(
                request.getRequestName(),
                request.getBid(),
                request.getDueDate(),
                foundUser
        );

        Request savedRequest = this.requestRepository.save(newRequest);
        return savedRequest;
    }

    @PutMapping("/{userId}/{id}")
    public Request updateRequest(
            @RequestBody Request request,
            @PathVariable("userId") ObjectId userId,
            @PathVariable("id") ObjectId id,
            HttpServletResponse response) throws ParseException {

        User foundUser = this.userRepository.findById(userId);
        if (!foundUser.isAdmin()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        Request foundRequest = this.requestRepository.findById(id);

        // change all fields of the request
        foundRequest.setRequestName(request.getRequestName());
        foundRequest.setBid(request.getBid());
        foundRequest.setDueDate(request.getDueDate());
        foundRequest.setComplited(request.isComplited());
        foundRequest.setCancelled(request.isCancelled());

        Request savedRequest = this.requestRepository.save(foundRequest);
        return savedRequest;
    }

    // works
    @DeleteMapping("/{userId}/{id}")
    public Request deleteRequest(@PathVariable("userId") ObjectId userId, @PathVariable("id") ObjectId id, HttpServletResponse response) {

        User foundUser = this.userRepository.findById(userId);
        if (!foundUser.isAdmin()) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        Request foundRequest = this.requestRepository.findById(id);
        this.requestRepository.delete(foundRequest);
        return foundRequest;
    }
}
