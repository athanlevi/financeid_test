package net.financeid.savinko.task;

import net.financeid.savinko.task.models.Request;
import net.financeid.savinko.task.models.User;
import net.financeid.savinko.task.repositories.RequestRepository;
import net.financeid.savinko.task.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.text.ParseException;

@Component
public class Seeder implements CommandLineRunner {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws ParseException {

        // clear database
        this.requestRepository.deleteAll();
        this.userRepository.deleteAll();

        User userSimple1 = new User("Вася", false);
        User userSimple2 = new User("Парамон", false);
        User userAdmin = new User("Петя", true);

        this.userRepository.save(userSimple1);
        this.userRepository.save(userSimple2);
        this.userRepository.save(userAdmin);

        Request req1 = new Request("Помыть слона", 100.00, "30/11/2018", userSimple1);
        Request req2 = new Request("Достать соседа", 90.00, "20/11/2018", userSimple1);
        Request req3 = new Request("Новая французская революция", 9_000_000_000_000.00, "01/01/2020", userSimple2);

        this.requestRepository.save(req1);
        this.requestRepository.save(req2);
        this.requestRepository.save(req3);


    }
}
