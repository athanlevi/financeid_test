package net.financeid.savinko.task.repositories;

import net.financeid.savinko.task.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findById(ObjectId id);
}
