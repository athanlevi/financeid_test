package net.financeid.savinko.task.repositories;

import net.financeid.savinko.task.models.Request;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends MongoRepository<Request, String> {

    Request findById(ObjectId id);

    List<Request> findRequestsByAuthor_Id(String id);

    List<Request> findRequestsByComplitedIsTrue();

    List<Request> findRequestsByComplitedIsFalse();

    List<Request> findRequestsByCancelledIsTrue();

    List<Request> findRequestsByCancelledIsFalse();
}
